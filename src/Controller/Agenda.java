package Controller;

import Model.BazaDeDate;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Feri
 */
public class Agenda {
    
    // <editor-fold defaultstate="collapsed" desc="Constructor">    
    public Agenda(){ 
          
       persoane = new ArrayList<Persoana>();
       
       evenimente = new ArrayList<Eveniment>();
       
       bDate = new BazaDeDate();
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Getter & Setter">
    public  void SetNumeAgenda(String nume){
        numeAgenda = nume;
    }
    
    public  String GetNumeAgenda(){
        return numeAgenda;
    }
    
    public void setProprietar(Proprietar proprietar){
        this.proprietar = proprietar;
    }
    public Proprietar getProprietar(){
        return proprietar;
    }
    
    public ArrayList<Persoana> getPersoane(){return persoane;}
    public ArrayList<Eveniment> getEvenimente(){return evenimente;}
    
    private void setPersoane(ArrayList<Persoana> persoane){ this.persoane = persoane;}
    private void setEvenimente(ArrayList<Eveniment> evenimente){ this.evenimente = evenimente;}
    // </editor-fold>   
    
    // <editor-fold defaultstate="collapsed" desc="Metode Publice">
    public void getAgenda(){
        
       for(String linie : bDate.getContinutBaza(numeAgenda))
       {
            if(!proprietarExista)
                incarcaProprietar(linie);
           
           incarcaCunostiinte(linie);
           
           incarcaEvenimente(linie);
       }
       
    }
    
    public void ModificaProprietar(String nume, String prenume, String telefon)
    {
        if(!nume.isEmpty())
            proprietar.setNume(nume);
        if(!prenume.isEmpty())
            proprietar.setPrenume(prenume);
        if(!telefon.isEmpty())
            proprietar.setTelefon(Integer.parseInt(telefon));
    }
    
    public void modificaPersoane(String[][] date){
        ArrayList<Persoana> obiect = new ArrayList<Persoana>();
        for(String[] persoana : date)
        {
            try{
            obiect.add(new Persoana(persoana[0], persoana[1], Integer.parseInt(persoana[2])));
            }catch(Exception e){System.out.println("The inserted value was not a string");}
        }
        
        this.setPersoane(obiect);
    }
    public void modificaEvenimente(String[][] date){
        ArrayList<Eveniment> obiect = new ArrayList<Eveniment>();
        for(String[] event : date)
        {
            try{
            obiect.add(new Eveniment(event[0], event[1], event[2],event[3]));
            }catch(Exception e){e.printStackTrace();}
        }
        
        this.setEvenimente(obiect);
    }
    
    
    public void SalvareModificariAgenda(Boolean val){
        if(val)
           salveazaAgenda(); 
    }
    
    public void InchidereConexiuneBD(Boolean val){
        if(val)
            bDate.InchideBD();
    }
    
    public void creareAgenda(String [] date)
    {
        String numeAgenda = date[0];
        Proprietar proprietarNou = new Proprietar(date[1], date[2], Integer.parseInt(date[3]), date[4], date[5]);
        
        ArrayList<String> textLocal = new ArrayList<String>();
        textLocal.add("nume : " + proprietarNou.getNume() + ", prenume : " + proprietarNou.getPrenume() + 
                ", telefon : " + proprietarNou.getTelefon() + 
                ", utilizator : " + proprietarNou.getUtilizator() + 
                ", parola : " + proprietarNou.getParola());
        textLocal.add(new String()); //adaugam linie goala , pt citire
        
        bDate.creareAgenda(numeAgenda, textLocal);
        
    }
    // </editor-fold>
       
    // <editor-fold defaultstate="collapsed" desc="Metode Private">
    private void incarcaProprietar(String line) throws NumberFormatException {
        Pattern pattern = Pattern.compile("(\\S+?)\\s*:\\s*([a-zA-Z0-9]+)");
        
        Matcher matcher = pattern.matcher(line);
        if(!line.isEmpty())
        {
            ArrayList<String> valori = new ArrayList<String>();
            
            while(matcher.find())
                valori.add(matcher.group(2));
            
            proprietar = new Proprietar(valori.get(0),valori.get(1),Integer.parseInt(valori.get(2)),
                                           valori.get(3),valori.get(4));
            //momentan Utilizator si parola nu sunt folosite
            
            
            proprietarExista = true;    
        }
    }
    
    
    private void incarcaCunostiinte(String line) throws NumberFormatException {
        Pattern pattern = Pattern.compile("\\S+? \\S+? \\d+");
        
        Matcher matcher = pattern.matcher(line);
        if(!line.isEmpty() && matcher.find())
        {
            String[] valori = line.split(" ");
            if(valori.length == 3)
                persoane.add(new Persoana(valori[0], valori[1], Integer.parseInt(valori[2])));
        }
    }
    
    private void incarcaEvenimente(String line) throws NumberFormatException {
        Pattern pattern = Pattern.compile("Eveniment,\\S+?,\\S+?,\\S+,(\\S*\\s)*");
        
        Matcher matcher = pattern.matcher(line);
        if(!line.isEmpty() && matcher.find())
        {
            String[] valori = line.split(",");
            if(valori.length == 4)
            
                //java.util.Calendar test = java.util.Calendar.getInstance();
                //test. = valori[1];
                
                evenimente.add(new Eveniment(valori[1], valori[2], valori[3]));
            
            else if(valori.length >= 5)
                evenimente.add(new Eveniment(valori[1], valori[2], valori[3],valori[4]));
            
        }
    }
    
    private void salveazaAgenda(){
        text = new ArrayList<String>();
        
        AdaugaProprietar();
        
        AdaugaCunostiinte();
        
        AdaugaEvenimente();//TODO : de facut
        
        bDate.ModificaBD(text);
    }
    
    private void AdaugaProprietar(){
        text.add("nume : " + proprietar.getNume() + ", prenume : " + proprietar.getPrenume() + 
                ", telefon : " + proprietar.getTelefon() + 
                ", utilizator : " + proprietar.getUtilizator() + 
                ", parola : " + proprietar.getParola());
        text.add(new String()); //adaugam linie goala , pt citire
    }
     
    private void AdaugaCunostiinte(){
        for(Persoana pers : getPersoane())
            text.add(pers.getNume() + " " + pers.getPrenume() + " " + pers.getTelefon());
        
    }
    
    private void AdaugaEvenimente(){
        for(Eveniment event : getEvenimente())
            text.add("Eveniment: " + event.getOra() + " " + event.getData() + "  " +
                    event.getLocatie() +  " " + event.getActivitate());
    
    
    }
    // </editor-fold>
     
 
    
    
    public static void main(String[] args)
    {
        Agenda ag = new Agenda();
        ag.getAgenda();
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="Variabile">
    private ArrayList<Eveniment> evenimente;
    private String numeAgenda;
    private ArrayList<Persoana> persoane;
    private Boolean proprietarExista = false;
    private Proprietar proprietar;
    private Model.BazaDeDate bDate;
    private ArrayList<String> text;
    // </editor-fold>
    
    

}
