/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;


/**
 *
 * @author Feri
 */
public class Eveniment {
    
    private String ora;
    private String data;
    private String locatie;
    private String activitate;
    
    public String getLocatie(){return locatie;}
    public String getData(){return data;}
    public String getOra(){return ora;}
    public String getActivitate(){return activitate;}
    
    public void setActivitate(String s){activitate = s;}
    
    public Eveniment(String ora, String data, String locatie){
        this(ora,data,locatie,null);
    }
    
    public Eveniment(String ora, String data, String locatie, String activitate){
        this.ora = ora;
        this.data = data;
        this.locatie = locatie;
        this.activitate = activitate;
    }
    

}
