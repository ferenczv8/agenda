package Controller;

/**
 * @author Feri
 */
public class Persoana {
    
    
    public String getNume(){return nume;}
    public void setNume(String nume){this.nume = nume;}
    
    public String getPrenume(){return prenume;}
    public void setPrenume(String nume){this.prenume = nume;}
    
    public Integer getTelefon(){return telefon;}
    public void setTelefon(Integer telefon){this.telefon = telefon;}
    
    public Persoana(){}
    public Persoana(String nume, String prenume, Integer telefon)
    {
        this.nume = nume;
        this.prenume = prenume;
        this.telefon = telefon;                
    }
    
    protected String nume;
    protected String prenume;
    protected Integer telefon;
    
    
}
