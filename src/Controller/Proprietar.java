package Controller;

/**
 *
 * @author Feri
 */
public class Proprietar extends Persoana{

    public Proprietar(){}
       
    public Proprietar(String nume, String prenume, Integer telefon, String utilizator, String parola)  {
        
        super(nume, prenume, telefon);
        
        this.parola = parola;
        
        this.utilizator = utilizator;
    }
    
    public String getUtilizator(){return utilizator;}
    public void setUtilizator(String utilizator){this.utilizator = utilizator;}
    
    
    public void setParola(String parola){this.parola = parola;}
    public String getParola(){return parola;}
    
    public Boolean VerificaProprietar(String utilizator, String parola)
    {
        if(this.parola.equalsIgnoreCase(parola) && this.utilizator.equalsIgnoreCase(utilizator))
            return true;
        
        return false;
    }
    
    public Boolean VerificaProlaUitata(String numeP, String utilizator){
        
        if(this.nume.equalsIgnoreCase(numeP) && this.utilizator.equalsIgnoreCase(utilizator))
            return true;
        
        return false;
    }
    
    private String utilizator;
    private String parola;
}
