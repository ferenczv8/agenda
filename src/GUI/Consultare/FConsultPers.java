package GUI.Consultare;

import Controller.Agenda;


public class FConsultPers extends javax.swing.JFrame {

   
    public FConsultPers(Agenda object) {
        if(object.GetNumeAgenda()!=null){
            agenda = object;
        
            Date();
        
            initComponents();
        }
    }
   
    @SuppressWarnings("unchecked") 
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        setTitle("Consultare date cunostinte");
        
        setSize(500, 500);
        setVisible(true);
        add(jps);
     
        pack();
    }// </editor-fold>                        

    public void Date()
    {
          String[] coloane = {"Nume", "Prenume", "Telefon"};
  
          String[][] date = new String[agenda.getPersoane().size()][];
          
          int contor = 0;
          for(Controller.Persoana p: agenda.getPersoane())
          {
              date[contor] = new String[3];
              date[contor][0] = p.getNume();
              date[contor][1] = p.getPrenume();
              date[contor][2] ="0" + p.getTelefon().toString();
              contor++;
          }    
  
          javax.swing.JTable jt = new javax.swing.JTable(date, coloane)
          {
               public boolean isCellEditable(int data, int columns)
               {
                   return false;
               }
   
               @Override
               public java.awt.Component prepareRenderer(javax.swing.table.TableCellRenderer r, int data, int columns)
               {
                   java.awt.Component c = super.prepareRenderer(r, data, columns);
    
                   if (data % 2 == 0)
                       c.setBackground(java.awt.Color.GRAY);
    
                   else
                       c.setBackground(java.awt.Color.LIGHT_GRAY);
    
                   return c;
               }
         };
  
  
         jt.setPreferredScrollableViewportSize(new java.awt.Dimension(450, 63));
         jt.setFillsViewportHeight(true);
  
         jps = new javax.swing.JScrollPane(jt);
         add(jps);
  
     }
    
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FConsultPers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FConsultPers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FConsultPers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FConsultPers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FConsultPers(null).setVisible(true);
            }
        });
    }
    
    private Agenda agenda; 
    private javax.swing.JScrollPane jps;
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
