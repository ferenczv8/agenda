package GUI.Consultare;

import Controller.Proprietar;
import Controller.Agenda;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class FConsultPropr extends javax.swing.JFrame {

    public FConsultPropr(Agenda agenda) {
        if(agenda.GetNumeAgenda()!=null)
        {
            proprietar = agenda.getProprietar();
        
            initComponents();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        setTitle("Consultare date proprietar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        
        getContentPane().setLayout(layout);
        
        field1  = new JTextField(proprietar.getNume(), 16);
        field1.setEditable(false);
        
        field2 = new JTextField(proprietar.getPrenume(), 16);
        field2.setEditable(false);
        
        field3 = new JTextField(proprietar.getTelefon().toString(),16);
        field3.setEditable(false);
        layout.setHorizontalGroup(layout.createSequentialGroup()
           .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                //.addGap(0, 100, Short.MAX_VALUE)
                .addComponent(label1)
                .addComponent(label2)
                .addComponent(label3))
                
           .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                //.addGap(0, 100, Short.MAX_VALUE)
                .addComponent(field1)
                .addComponent(field2)
                .addComponent(field3))
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
           .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                //.addGap(0, 60, Short.MAX_VALUE)
                .addComponent(label1)
                .addComponent(field1))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                //.addGap(0, 60, Short.MAX_VALUE)
                .addComponent(label2)
                .addComponent(field2))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                //.addGap(0, 60, Short.MAX_VALUE)
                .addComponent(label3)
                .addComponent(field3))
        );
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);
        pack();
    }// </editor-fold>


    public static void main(String args[]) {
	
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FConsultPropr.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FConsultPropr.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FConsultPropr.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FConsultPropr.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FConsultPropr(null).setVisible(true);
            }
        });
    }
    
    
    private Proprietar proprietar = null;
    private JLabel label1 = new JLabel("Nume:");
    private JTextField field1;
    private JLabel label2 = new JLabel("Prenume:");
    private JTextField field2;
    private JLabel label3 = new JLabel("Telefon:");
    private JTextField field3 ;

}
