package GUI;

import GUI.Modificare.FCunostinte;
import GUI.Modificare.FActEven;
import GUI.Modificare.FProprietar;
import GUI.Consultare.FConsultActEven;
import GUI.Consultare.FConsultPers;
import GUI.Consultare.FConsultPropr;
import Controller.Agenda;
import GUI.Utilizator.FUtilizatorParolaUitata;
import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FPrinc extends javax.swing.JFrame {

    // <editor-fold defaultstate="collapsed" desc="Proprietati">  
    private Agenda agenda;
    
    public void initializareAgenda(){
        agenda = new Agenda();
    }
    public Agenda getAgenda(){
        return agenda;
    }
    
    public FPrinc() {
        
        initializareAgenda();
        
        initComponents();
    }
    // </editor-fold>  
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Agenda personala");

       
        
        jMenu1.setText("Operatii la nivel de agenda");

        jMenuItem1.setText("Selectare agenda curenta");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Creare agenda noua");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setText("Salvare agenda");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setText("Inchidere agenda");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Actualizare agenda");

        jMenuItem5.setText("Actualizare proprietar");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem5);

        jMenuItem6.setText("Actualizare persoane cunoscute");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem6);

        jMenuItem7.setText("Actualizare activitati/evenimente");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem7);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Consultare agenda");

        jMenuItem8.setText("Consultare date proprietar");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem8);

        jMenuItem9.setText("Consultare date despre cunostinte");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem9);

        jMenuItem10.setText("Consultare date despre activitati/evenimente");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem10);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 279, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                                             

    // <editor-fold defaultstate="collapsed" desc="Operatii Agenda">  
    // <editor-fold defaultstate="collapsed" desc="Selectare Agenda">  
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {  
        this.initializareAgenda();
        
        FSelectareAgenda frame = new FSelectareAgenda(this);
        frame.setVisible(true);          
        
        WaitForAgenda(frame);
    }

    private void WaitForAgenda(FSelectareAgenda frame) {
        
        
        frame.getButton().addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                    
                System.out.println("Listened : Agenda was selected");
                ContinutAgendaSelectata();
            }
        }); 
        
    }
    
    // </editor-fold> 
    
    // <editor-fold defaultstate="collapsed" desc="Creare Agenda">  
    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) { 
        // TODO add your handling code here:
        new FCreareAgenda(agenda).setVisible(true);
    }// </editor-fold>   

    // <editor-fold defaultstate="collapsed" desc="Salvare Agenda">  
    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) { 
        // TODO add your handling code here:
        agenda.SalvareModificariAgenda(true);
    } // </editor-fold>  

    // <editor-fold defaultstate="collapsed" desc="Inchidere Agenda">  
    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) { 
        // TODO add your handling code here:
        agenda.InchidereConexiuneBD(true);
        System.exit(0);
    } // </editor-fold>  
    // </editor-fold>  
    
    // <editor-fold defaultstate="collapsed" desc="Editari Agenda">  
    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) { 
        // TODO add your handling code here:
        if(utilizatorLogat)
            new FProprietar(agenda).setVisible(true);
    } 

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) { 
        // TODO add your handling code here:
        if(utilizatorLogat)
            new FCunostinte(this).setVisible(true);
        
    } 

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) { 
        // TODO add your handling code here:
        if(utilizatorLogat)
            new FActEven(this).setVisible(true);
    } 
    // </editor-fold>  
    
    // <editor-fold defaultstate="collapsed" desc="Afisari">  
    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) { 
        // TODO add your handling code here:
        if(utilizatorLogat)
            new FConsultPers(agenda);//.setVisible(true);
    } 

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        if(utilizatorLogat)
            new FConsultPropr(agenda).setVisible(true);
    }

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        if(utilizatorLogat)
            new FConsultActEven(agenda).setVisible(true);
    }
    // </editor-fold>  
    
    // <editor-fold defaultstate="collapsed" desc="Loginare">                                      

    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt) {                                               
        // TODO add your handling code here:
        String parolaConvertita = new String(parola.getPassword());
        if(agenda.getProprietar().VerificaProprietar(utilizator.getText(), parolaConvertita))
        {
            utilizatorLogat = true;
            ContinutUtilizatorLogat();        
        }
    }                                              

    private void userNouButtonActionPerformed(java.awt.event.ActionEvent evt) {                                               
        // TODO add your handling code here:
        //new FCreareAgenda().setVisible(true);
    }                                              

    private void parolaButtonActionPerformed(java.awt.event.ActionEvent evt) {                                               
        // TODO add your handling code here:
        new FUtilizatorParolaUitata(agenda).setVisible(true);
    }  
    // </editor-fold>  
    
    // <editor-fold defaultstate="collapsed" desc="Fereastra Principala">  
    private void ContinutUtilizatorLogat(){ 
        getContentPane().removeAll();
     
        //eliberez valorile utilizator si parola
        utilizator.setText(new String());
        parola.setText(new String());
            
        javax.swing.JLabel loggedLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        loggedLabel.setText("Bine ai Venit");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(170, 170, 170)
                .addComponent(loggedLabel)
                .addContainerGap(172, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(145, Short.MAX_VALUE)
                .addComponent(loggedLabel)
                .addGap(141, 141, 141))
        );

        pack();
    }
    
    private void ContinutAgendaSelectata(){
        
        getContentPane().removeAll();
                
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        utilizator = new javax.swing.JTextField();
        parola = new javax.swing.JPasswordField();
        denumireAgenda = new javax.swing.JTextField();
        loginButton = new javax.swing.JToggleButton();
        //userNouButton = new javax.swing.JToggleButton();
        parolaButton = new javax.swing.JToggleButton();
        
        jLabel1.setText("Utilizator");

        jLabel2.setText("Parola");


        denumireAgenda.setText(agenda.GetNumeAgenda());
        denumireAgenda.setEditable(false);
        

        loginButton.setText("LogIn");
        loginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginButtonActionPerformed(evt);
            }
        });

        //userNouButton.setText("Creare Utilizator");
//        userNouButton.addActionListener(new java.awt.event.ActionListener() {
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                userNouButtonActionPerformed(evt);
//            }
//        });
        parolaButton.setText("Parola Uitata");
        parolaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                parolaButtonActionPerformed(evt);
            }
        });
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(denumireAgenda, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 97, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(parolaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(loginButton, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                        .addComponent(parola)
                        .addComponent(utilizator)))
                    //.addComponent(userNouButton, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(62, 62, 62))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(utilizator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE))
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(parola, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addComponent(loginButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(parolaButton)
                    .addComponent(denumireAgenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
               // .addComponent(userNouButton)
                .addGap(54, 54, 54))
        );
        if(agenda.getProprietar()!=null)
        System.out.print("Utilizator : " + agenda.getProprietar().getUtilizator() +"\n" + 
                         "Parola : " + agenda.getProprietar().getParola());
    }
    
    // </editor-fold>  
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FPrinc.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FPrinc.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FPrinc.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FPrinc.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        
        new FPrinc().setVisible(true);
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FPrinc().setVisible(true);
//            }
//        });
    }

     
    // <editor-fold defaultstate="collapsed" desc="Variabile">                  
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPasswordField parola;
    private javax.swing.JTextField utilizator;
    private javax.swing.JTextField denumireAgenda;
    private javax.swing.JToggleButton loginButton;
   // private javax.swing.JToggleButton userNouButton;
    private javax.swing.JToggleButton parolaButton;
    private Boolean utilizatorLogat = false;
   //</editor-fold>
}
