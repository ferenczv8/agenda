package GUI;
import Controller.Agenda;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.*;

/**
 * @author Feri
 */
public class FSelectareAgenda extends JFrame{
    
    public FSelectareAgenda(FPrinc object)
    {
        parinteObiect = object;
        
        this.agenda = object.getAgenda();
        
        continut();
        
        initComponents();
    }
    public JButton getButton(){return jButton2;};
    
    private void initComponents()
    {
        setTitle("Selectare Agenda");
        
        JPanel panel = new JPanel();
        JLabel label = new JLabel("Selecta-ti agenda dorita");
        
        panel.add(label);
        panel.add(jScrollPanel);
        
        
        jButton1 = new javax.swing.JButton();
        jButton1.setText("Salvare");//Salveaza alegerea
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonActionPerformed(evt);
            }
        });
        jButton2 = new javax.swing.JButton();
        jButton2.setText("Terminare");//Salveaza alegerea
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        
        panel.add(jButton1);
        panel.add(jButton2);

        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(jButton1)
                        .addGap(84, 84, 84)
                        .addComponent(jButton2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(132, 132, 132)
                        .addComponent(label, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(jScrollPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(70, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(label)
                .addGap(18, 18, 18)
                .addComponent(jScrollPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addGap(25, 25, 25))
        );

        pack();
    }
    
     private void jButtonActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
       
        agenda.SetNumeAgenda(agendaAleasa);
                
        agenda.getAgenda();
    }
     
     private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        setVisible(false);
    }
    
    private void continut(){
      
        myList = new JList(new Model.BazaDeDate().ListaAgende());//obtine lista de agende existente
        jScrollPanel = new JScrollPane(myList);
        myList.addListSelectionListener(listener);
    }
    
    

    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {               
                FSelectareAgenda p = new FSelectareAgenda(null);               
            }
        });
    }
    
    private JButton jButton1;
    private JButton jButton2;
    private JScrollPane jScrollPanel;
    private JList myList;
    private ArrayList<String> Fisiere;
    private Agenda agenda;
    private String agendaAleasa;
    private FPrinc parinteObiect;
    private ListSelectionListener listener = new ListSelectionListener()
    {
        @Override
        public void valueChanged(ListSelectionEvent evt)
        {
            if(evt.getValueIsAdjusting())
            {
                System.out.println("Eventhandler called evt.getValueIsAdjusting() true");
                return;
            }
            else
            {
                System.out.println("Eventhandler called myList.getSelectedValue() =" + myList.getSelectedValue());
                
                //TODO : salvez numele fisierului
                agendaAleasa = myList.getSelectedValue().toString();
                
            }
        }
    };
}