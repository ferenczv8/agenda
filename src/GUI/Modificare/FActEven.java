package GUI.Modificare;

import Controller.Agenda;
import GUI.FPrinc;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FActEven extends javax.swing.JFrame {

    
    public FActEven(FPrinc object) {
        date(object);
        
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        adaugareButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        modificaActivitate = new javax.swing.JButton();

        setTitle("Actualizare activitati/evenimente");

        coloane = new String [] {
            "Ora", "Data", "Locatie"
        };
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            date,coloane
        ));
        jScrollPane1.setViewportView(jTable1);

        adaugareButton.setText("Adauga");
        adaugareButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adaugareButtonActionPerformed(evt);
            }
        });

        deleteButton.setText("Sterge");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        modificaActivitate.setText("Modifica Activitate");
        modificaActivitate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificaActivitateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(modificaActivitate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deleteButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(adaugareButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(80, 80, 80))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(adaugareButton)
                        .addGap(65, 65, 65)
                        .addComponent(deleteButton)
                        .addGap(79, 79, 79)
                        .addComponent(modificaActivitate)))
                .addContainerGap(52, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void modificaActivitateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificaActivitateActionPerformed
        // TODO add your handling code here:
        int test = jTable1.getSelectedRow();
        String text = date[jTable1.getSelectedRow()][3];
        
        FActivitate frame= new FActivitate(true, text,parinteObiect.getAgenda(), jTable1.getSelectedRow());
        frame.setVisible(true);
        
        frame.getButton().addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                    
                System.out.println("Listened : terminare click!");
                
                date(parinteObiect);
                
                jTable1.setModel(new DefaultTableModel(date,coloane));
            }
        });
    }//GEN-LAST:event_modificaActivitateActionPerformed

    private void adaugareButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adaugareButtonActionPerformed
        // TODO add your handling code here:
        String ora = JOptionPane.showInputDialog("Adauga Ora");
        String data = JOptionPane.showInputDialog("Adauga Data");
        String locatie  = JOptionPane.showInputDialog("Adauga Locatie");
        
        String[][] dateVechi = date;
        date = new String[dateVechi.length + 1][coloane.length + 1];
        
        
        int contor = 0;
        for( String[] val: dateVechi)
        {
            date[contor] = new String[4];
            date[contor++] = val;
        }
        date[date.length -1] = new String[]{ora,data,locatie," "};
        
        jTable1.setModel(new DefaultTableModel(date,coloane));
        
        parinteObiect.getAgenda().modificaEvenimente((String[][]) date);
    }//GEN-LAST:event_adaugareButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        // TODO add your handling code here:
        String locatie = JOptionPane.showInputDialog("Adauga Locatia Evenimentului");
        
        String[][] dateVechi = date;
        date = new String[dateVechi.length - 1][4];
        
        
        int contor = 0;
        for( String[] val: dateVechi)
        {
            if(!val[2].equals(locatie))
            {
                date[contor] = new String[4];
                date[contor++] = val;
            }
        } 
        jTable1.setModel(new DefaultTableModel(date,coloane));
        
        parinteObiect.getAgenda().modificaEvenimente(date);
    }//GEN-LAST:event_deleteButtonActionPerformed

     private void date(FPrinc object){
            
        parinteObiect = object;
        
        Agenda agenda = parinteObiect.getAgenda();
        
        date = new String[agenda.getEvenimente().size()][4];
          
        int contor = 0;
        for(Controller.Eveniment event: agenda.getEvenimente())
             date[contor++] = new String[]{event.getOra(),
                                            event.getData(),
                                            event.getLocatie(),
                                            event.getActivitate()};
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FActEven.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FActEven.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FActEven.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FActEven.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FActEven(null).setVisible(true);
            }
        });
    }

    private String[][] date;
    private String[] coloane;
    private FPrinc parinteObiect;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton adaugareButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton modificaActivitate;
    // End of variables declaration//GEN-END:variables
}
