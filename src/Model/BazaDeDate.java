package Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Feri
 */
public class BazaDeDate {
    
    private Scanner DeschideBD(){
        try
        {
           return new Scanner(new File(numeAgenda));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
    
    public ArrayList<String> getContinutBaza(String numeAgenda){
       
       this.numeAgenda = numeAgenda;
       
       sc = DeschideBD();
       
       ArrayList<String> rezultat = new ArrayList<String>();
       
       while(sc.hasNext())
       {
           String line =  sc.nextLine();
           
           if(!line.isEmpty())
               rezultat.add(line);
           
       }
       
       return rezultat;
        
    }
    
    public void InchideBD(){
        sc.close();
    }
    
    public void ModificaBD(ArrayList<String> text){
        try{
            PrintWriter pw = new PrintWriter(new FileWriter(new File(numeAgenda)));
            
            for(String linie : text)
            {
                pw.println(linie);
                pw.flush();
            }
            
            pw.close();
            
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
    
    
    private void VerificaFisiere(){
        Fisiere = new ArrayList<String>();
        File folder = new File("D:\\AgendaPersonala");
        File[] listOfFiles = folder.listFiles();

        for(File file : folder.listFiles())
            if (file.isFile() && file.getName().endsWith(".txt")) 
                 Fisiere.add(file.getName());

    }
    
    
    public Object[] ListaAgende(){
        VerificaFisiere();
        
        int randuri = Fisiere.size();
        Object[] listData = new String[randuri];
        for(int i =0; i < randuri; i++)
            listData[i]= Fisiere.get(i);
        
        return listData;
    }
    
    public void creareAgenda(String nume, ArrayList<String> text){
        File agenda = new File(nume + ".txt");
        if(!agenda.exists())
        {
            try {
                agenda.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(BazaDeDate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //new FileOutputStream("score.txt", true).close();
        //https://stackoverflow.com/questions/9620683/java-fileoutputstream-create-file-if-not-exists
        
        String aux = numeAgenda;
        
        numeAgenda = nume+ ".txt";
        
        ModificaBD(text);
        
        numeAgenda = aux;
    }
    
    private Scanner sc;
    private String numeAgenda;
    
    private ArrayList<String> Fisiere;
}
